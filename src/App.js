import {BrowserRouter, Routes, Route} from 'react-router-dom';
import Home from './components/Home';
import Members from './components/Members'
import SignIn from './components/SignIn';
import Shop from './components/Shop';

function App() {
  return (

   <>
    <BrowserRouter>
      <Routes>

        <Route path='/' element={<Home />} />
        <Route path='/members' element={<Members />} />
        <Route path='/signin' element={<SignIn />} />
        <Route path='/shop' element={<Shop />} />

      </Routes>
    </BrowserRouter>

   </>
    
  );
}

export default App;
