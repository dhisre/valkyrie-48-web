import Navbar from './Navbar'
import React from 'react'
import {AiOutlineShoppingCart
        } from 'react-icons/ai'
import Shirt from '../assets/shirt.png'
import Footer from './Footer'

const Shop = () => {
    const products = [
        {
          id: 1,
          name: 'Official Jersey Valkyrie',
          href: '#',
          price: 'Rp. 650.000',
          imageSrc: 'https://hippocketmornington.com.au/wp-content/uploads/2019/12/2CJ-JBs-Colours-of-cotton-Charcoal-marle-polo.jpg',
          imageAlt: 'Official Jersey Member of Valkyrie with Signature',
        },
        {
          id: 2,
          name: 'Official Hoodie Valkyrie',
          href: '#',
          price: 'Rp. 1.300.000',
          imageSrc: 'https://hippocketmornington.com.au/wp-content/uploads/2019/12/AS-Colour-trade-5101-supply-hoody-navy.jpg',
          imageAlt: 'Official Hoodie Member of Valkyrie with Signature',
        },
        {
          id: 3,
          name: 'Valkyrie Tumbler',
          href: '#',
          price: 'Rp. 300.000',
          imageSrc: 'https://images.tokopedia.net/img/cache/900/VqbcmM/2021/7/23/acbf00b7-8037-44f9-abf8-3813fb49200f.jpg',
          imageAlt: 'Exclusive Valkyrie Tumblr',
        },
        {
          id: 4,
          name: 'Valkyrie Deskmat',
          href: '#',
          price: 'Rp. 300.000',
          imageSrc: 'https://resource.logitech.com/w_800,c_lpad,ar_1:1,q_auto,f_auto,dpr_1.0/d_transparent.gif/content/dam/logitech/en/products/mice/desk-mat-studio-series/gallery/logitech-desk-mat-studio-series-corner-view-mid-grey.png?v=1',
          imageAlt: 'Smooth & Friendly Deskmat for ur Gaming Accessory',
        },
        {
            id:5,
            name:'Razer x Valkyrie Limited Edition Headphones',
            href:'#',
            price:'Rp. 3.500.000',
            imageSrc:'https://cf.shopee.co.id/file/4dc5ce9a3855192354df581cc6e5b87a',
            ImageAlt:'Limited Collaboration of Valkyrie & Razer'
        }
        // More products...
      ]
  return (
    <>
<Navbar />

    <div className="bg-purple-400">
      <div className="mx-auto max-w-2xl py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8 text-center py-8 text-slate-300">
            <h2 className='text-3xl font-bold text-purple'>Valkyrie Shop</h2>
            <h3 className='text-5xl font-bold text-purple py-8 px-12 '>Get Your Limitid Merch Here!</h3>

        <div className="grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
          {products.map((product) => (
            <a key={product.id} href={product.href} className="group">
              <div className="aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-w-7 xl:aspect-h-8">
                <img
                  src={product.imageSrc}
                  alt={product.imageAlt}
                  className="h-full w-full object-cover object-center group-hover:opacity-75"
                />
              </div>
              <h3 className="mt-4 text-sm text-white">{product.name}</h3>
              <p className="mt-1 text-lg font-medium text-white">{product.price}</p>
            </a>
          ))}
        </div>
      </div>
    </div>
   
        <Footer />
</>
  )

}

    export default Shop
