import React, {useState} from "react";
import {MenuIcon, XIcon} from '@heroicons/react/outline'

const Navbar = () => {

    const [nav, setNav] = useState(false)
    const handleClick = () => setNav(!nav)

    return (
        <div className="w-screen h-[80px] z-10 bg-pink-300 fixed drop-shadow-lg">
            <div className="='px-2 flex justify-between items-center w-full h-full">
                <div className="flex items-center">
                    <h1 className="text-3xl font-bold mr-4 ml-4 sm:text-4xl text-white"><a href="/"> Valkyrie 48 </a></h1>
                    <ul className="hidden md:flex text-white">
                        <li><a href="/members"> Member </a></li>
                        <li><a href="https://www.youtube.com/user/JKT48"> Stream </a></li>
                        <li><a href="/shop"> Shop </a></li>
                    </ul>
                </div>
                <div className="hidden md:flex pr-4">
                    <button className="border-none bg-transparent text-white mr-4"><a href="/signin"> Sign In </a></button>
                    <button className="px-8 py-3">Sign Up</button>
                </div>
                <div className="md:hidden" onClick={handleClick}>
                    {!nav ? <MenuIcon className='w-5' /> : <XIcon className="w-5" /> }
                
                </div>
            </div>
                <ul className={!nav ?'hidden' : 'absolute bg-pink-100 w-full px-8'}>
                <li className="border-b-2 border-pink-300 w-full"><a href="/">Home</a></li>
                        <li className="border-b-2 border-pink-300 w-full"><a href="/members"> Member </a></li>
                        <li className="border-b-2 border-pink-300 w-full"><a href="https://www.youtube.com/user/JKT48"> Stream </a></li>
                        <li className="border-b-2 border-pink-300 w-full"><a href="/shop"> Shop </a></li>
                <div className="flex flex-col my-4">
                    <button className="bg-transparent text-indigo-600 px-8 py-3 mb-4"><a href="/signin">Sign In</a></button>
                    <button className="px-8 py-3">Sign Up</button>
                </div>
                </ul>
        </div>


        
    )
}

export default Navbar