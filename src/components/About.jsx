import React from "react";

const About = () => {

    return (
        <div className="w-full my-32">
            <div className="mas-w-[1240px] mx-auto">
                <div className="text-center">
                    <h2 className="text-5xl font-bold">Winner Of All Time</h2>
                    <p className="text-3xl py-6 text-gray-500">Valkyrie48 is here as an "Entertaining Gaming Girl" 
                                which aims to become a symbol of E-Sports in Indonesia and expand it to all over Asia and the world.<br></br>
                                The members of Valkyrie48 are female knights who are tough but still beautiful and charming 
                                who will entertain you with interesting content on your favorite social media. <br />
                                Keep up with our activities!</p>
                </div>
                
                <div className="grid md:grid-cols-3 gap-1 px-2 text-center">
                    <div className="border py-8 rounded-xl shadow-xl">
                        <p className="text-6xl font-bold text-indigo-600">3 Thropy</p>
                        <p className="text-grey-400 mt-2">Mobile Legends</p>
                    </div>
                    <div className="border py-8 rounded-xl shadow-xl">
                        <p className="text-6xl font-bold text-indigo-600">8 Thropy</p>
                        <p className="text-grey-400 mt-2">Dota 2 Major Championship</p>
                    </div>
                    <div className="border py-8 rounded-xl shadow-xl">
                        <p className="text-6xl font-bold text-indigo-600">4 Thropy</p>
                        <p className="text-grey-400 mt-2">Valorant Champions</p>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default About