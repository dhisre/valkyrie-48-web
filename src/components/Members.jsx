import Navbar from "./Navbar";
import About from "./About";
import Footer from "./Footer"

const Members = () => {

    const member = [
        {
            id: 1,
            name: 'Tan Zhi Hui Celline',
            href: 'https://www.instagram.com/celine.chenzw/',
            Division: 'Dota 2',
            ImageSrc: 'https://i.pinimg.com/originals/d1/64/09/d16409c729e9f07c6b109f9a469f27a9.jpg',
            ImageAlt: '',
        },
        {
            id: 2,
            name: 'Zahra Khaulah',
            href: 'https://www.instagram.com/itszahranur/',
            Division: 'Dota 2',
            ImageSrc: 'https://i02.appmifile.com/393_bbs_en/10/11/2020/3875d83798.jpg',
            ImageAlt: '',
        },
        {
            id: 3,
            name: 'Desy Genoveva',
            href: 'https://www.instagram.com/desygenoveva_/',
            Division: 'Dota 2',
            ImageSrc: 'https://i02.appmifile.com/254_bbs_en/10/11/2020/97d045dc81.jpg',
            ImageAlt: '',
        },
        {
            id: 4,
            name: 'Jinan Safa Safira',
            href: 'https://www.instagram.com/jkt48jinan/',
            Division: 'Dota 2',
            ImageSrc: 'https://i.pinimg.com/736x/dc/24/b7/dc24b728a33d3f0209aa2944539251ec.jpg',
            ImageAlt: '',
        },
        {
            id: 5,
            name: 'Anindita Cahyadi',
            href: 'https://www.instagram.com/andth.rc/',
            Division: 'Dota 2',
            ImageSrc: 'https://i02.appmifile.com/12_bbs_en/10/11/2020/73cf6bff8f.jpg',
            ImageAlt: '',
        },
        {
            id: 6,
            name: 'Coach 1',
            href: '#',
            Division: 'Coach',
            ImageSrc: 'https://thumbs.dreamstime.com/b/user-icon-flat-style-person-icon-user-icon-web-site-user-icon-vector-illustration-user-icon-flat-style-person-icon-user-129831161.jpg',
            ImageAlt: '',
        },
        {
            id: 7,
            name: 'Coach 2',
            href: '#',
            Division: 'Coach',
            ImageSrc: 'https://thumbs.dreamstime.com/b/user-icon-flat-style-person-icon-user-icon-web-site-user-icon-vector-illustration-user-icon-flat-style-person-icon-user-129831161.jpg',
            ImageAlt: '',
        },
        {
            id: 8,
            name: 'Manager',
            href: '#',
            Division: 'Manager Teams',
            ImageSrc: 'https://thumbs.dreamstime.com/b/user-icon-flat-style-person-icon-user-icon-web-site-user-icon-vector-illustration-user-icon-flat-style-person-icon-user-129831161.jpg',
            ImageAlt: '',
        },
        
    ]

    return (
     <>
     <Navbar />
     
     <div   className="bg-purple-400">
        <div    className="mx-auto max-w-2xl py-16 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8 text-center py-8 text-slate-300">
            <h2     className="text-4xl font-bold text-white mb-6 mt-4"> This Is Our Team </h2>
            
            <div className="grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 xl:gap-x-8">
                {member.map((member) => (
                    <a key={member.id} href={member.href} className="group">
                        <div className="aspect-w-1 aspect-h-1 w-full overflow-hidden rounded-lg bg-gray-200 xl:aspect-w-7 xl:aspect-h-8">
                            <img 
                            src={member.ImageSrc}
                            alt={member.ImageAlt}
                            className="h-full w-full object-cover object-center group-hover:opacity-75"
                            />
                        </div>
                        <h2     className="mt-4 text-lg text-white">{member.name}</h2>
                        <h3     className="mt-1 text-lg font-medium text-white">{member.Division}</h3>
                    </a>
                ))}

            </div>
        </div>

     </div>
    
                <About />
                <Footer />
     </>        
    )
}
export default Members