import React from "react";
import Navbar from './Navbar';
import Hero from './Hero';
import About from './About';
import Footer from "./Footer";

const Home = () => {
    return (
    <>
    <Navbar />
    <Hero />
    <About />
    <Footer />
    </>
    )
}

export default Home