import React from "react";
import {
    AiFillFacebook,
    AiFillInstagram,
    AiFillTwitterCircle,
    AiFillYoutube
} from 'react-icons/ai'

import bgImg from '../assets/valkyrie.png'


const Hero = () => {

    return(
        <div className="w-full h-screen bg-purple-100 flex flex-col justify-between">
            <div className="grid md:grid-cols-2 max-w-[1240px] m-auto">
                <div className="flex flex-col justify-center md:items-start w-full px-2 py-8">
                    <p className="text-2xl "> JKT48 E-Sport Team </p>
                    <h1 className="py-3 text-5xl md:text-7xl font-bold">Valkyrie 48</h1>
                    <p className="text-2xl">This is our Game</p>
                    <button className="py-3 px-6 sm:w-[60%] my-4 "><a href="https://www.youtube.com/user/JKT48">Stream Now </a> </button>
                </div>
                <div>
                    <img className="w-full" src={bgImg} alt="/" />
                </div>
                <div className="absolute flex flex-col py-8 md:min-w-[760px] bottom-[5%]
                                mx-1 md:left-1/2 transform md:-translate-x-1/2 bg-purple-100 
                                borderborderslate-300 rounded-xl text-center shadow-xl">
                    <p className="mb-2">Our Social Media</p>
                    <div className="flex justify-between flex-wrap px-4">
                        <p className="flex px-4 py-2 text-slate-500"><a href="https://twitter.com/officialjkt48"><AiFillTwitterCircle  className="h-6 text-purple-600" /> Twitter</a> </p>
                        <p className="flex px-4 py-2 text-slate-500"><a href="https://www.youtube.com/user/JKT48"><AiFillYoutube className="h-6 text-purple-600" /> Youtube</a> </p>
                        <p className="flex px-4 py-2 text-slate-500"><a href="https://www.instagram.com/willprkso/"><AiFillInstagram className="h-6 text-purple-600" /> Instagram</a> </p>
                        <p className="flex px-4 py-2 text-slate-500"><a href="#"><AiFillFacebook className="h-6 text-purple-600" /> Facebook</a>  </p>
                    </div>
                </div>
            </div>
            
        </div>
    )
}

export default Hero